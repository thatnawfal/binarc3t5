package com.thatnawfal.binarc3t5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

class LinearFilmAdapter : RecyclerView.Adapter<LinearFilmAdapter.LinearFilmViewHolder>() {
    private var diffCallback = object: DiffUtil.ItemCallback<Film>(){
        override fun areItemsTheSame(oldItem: Film, newItem: Film): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Film, newItem: Film): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private var differ = AsyncListDiffer(this, diffCallback)
    fun submitData(value: ArrayList<Film>) = differ.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinearFilmViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_linear_film, parent, false)
        return LinearFilmViewHolder(view)
    }

    override fun onBindViewHolder(holder: LinearFilmViewHolder, position: Int) {
        val film = differ.currentList[position]

        with(holder) {
            tvTitle.text = film.title
            ivPoster.setImageResource(film.poster)
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class LinearFilmViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle: TextView = itemView.findViewById(R.id.itemLinear_tvTitle)
        var ivPoster: ImageView = itemView.findViewById(R.id.itemLinear_ivPoster)
    }
}