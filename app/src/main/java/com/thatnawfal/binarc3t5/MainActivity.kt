package com.thatnawfal.binarc3t5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    val listFilm = arrayListOf(
        Film("Miracle in cell No.7", R.drawable.poster_micn),
        Film("Hajime No Ippo: Champions Road", R.drawable.poster_hnicr),
        Film("Stranger Things", R.drawable.poster_st),
        Film("Avangers: Endgame", R.drawable.poster_endgame)
    )
    private lateinit var rvContact: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvContact = findViewById(R.id.rv_contact)

        val listContact = arrayListOf(
            MyContact("Sabrina", "08197823893"),
            MyContact("Pak De", "082143897645"),
            MyContact("Pak Lee", "087854326677"),
            MyContact("Derek", "089078357825"),
            MyContact("Nawfal", "082143883436"),
            MyContact("Kido", "082846759988"),
            MyContact("Arif", "086548979999")
        )

        setLinearRecycler(listContact)

    }

    private fun setLinearRecycler(value: ArrayList<MyContact>) {
        val adapter = ContactAdapter(value)
        adapter.submitData(value)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvContact.layoutManager = layoutManager
        rvContact.adapter = adapter

    }

    private fun setGridRecycler() {
        val adapter = FilmAdapter(listFilm)
        val layoutManager = GridLayoutManager(this, 2)
        rvContact.layoutManager = layoutManager
        rvContact.adapter = adapter
    }

    private fun setFilmLinearRecycler() {
        val adapter = LinearFilmAdapter()
        adapter.submitData(listFilm)
        rvContact.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvContact.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_grid -> {
            setGridRecycler()
            true
        }
        R.id.menu_list -> {
            setFilmLinearRecycler()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }


}