package com.thatnawfal.binarc3t5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList

class ContactAdapter(private val list: ArrayList<MyContact>)
    : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    private var diffCallback = object: DiffUtil.ItemCallback<MyContact>(){
        override fun areItemsTheSame(oldItem: MyContact, newItem: MyContact): Boolean {
            return oldItem.nama == newItem.nama
        }

        override fun areContentsTheSame(oldItem: MyContact, newItem: MyContact): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private var differ = AsyncListDiffer(this, diffCallback)
    fun submitData(value: ArrayList<MyContact>) = differ.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_my_contact, parent, false)
        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = differ.currentList[position]

        holder.tvName.text = contact.nama
        holder.tvNohp.text = contact.noHp
    }

    override fun getItemCount(): Int = differ.currentList.size

    class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.findViewById(R.id.tvItem_name)
        val tvNohp: TextView = itemView.findViewById(R.id.tvItem_noHp)
    }
}