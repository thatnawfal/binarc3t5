package com.thatnawfal.binarc3t5

data class Film(
    val title: String,
    val poster: Int
)
