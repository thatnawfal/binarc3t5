package com.thatnawfal.binarc3t5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class FilmAdapter(private val list: ArrayList<Film>): RecyclerView.Adapter<FilmAdapter.GridViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_grid_film, parent, false)
        return GridViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        val film = list[position]

        holder.tvTitle.text = film.title
        holder.ivPoster.setImageResource(film.poster)
    }

    override fun getItemCount(): Int = list.size

    inner class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle: TextView = itemView.findViewById(R.id.gridItem_tvTitle)
        var ivPoster: ImageView = itemView.findViewById(R.id.gridItem_ivPoster)
    }
}